﻿using FlightSearch.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Airports
{
    public class AirportsDataAccess : IAirportsDataAccess
    {

        private readonly IFlightSearchDbContextFactory _factory;

        public AirportsDataAccess(IFlightSearchDbContextFactory factory)
        {
            _factory = factory;
        }

        public async Task<List<Airport>> FindAllAsync()
        {
           using (var context = _factory.Create())
            {
                var airports =  await context.Airports
                    .Include(c => c.Country)
                    .ToListAsync();

                return airports;
            }
        }

        public async Task<Airport> FindById(int id)
        {
            using (var context = _factory.Create())
            {
                var airport = await context.Airports
                    .Include(c => c.Country)
                    .Where(f => f.AirportId == id).FirstOrDefaultAsync();

                return airport;
            }
        }
    }
}
