﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FlightSearch.DataAccess
{
    public class FlightSearchDbContextFactory : IFlightSearchDbContextFactory
    {
        private readonly IConfiguration _configuration;

        public FlightSearchDbContextFactory() { }

        public FlightSearchDbContextFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // to do: chould be only one create method
        public IFlightSearchDbContext Create()
        {
            return new FlightSearchDbContext(_configuration, new DbContextOptionsBuilder<FlightSearchDbContext>().Options);
        }

        public FlightSearchDbContext CreateDbContext(string[] args)
        {
            return new FlightSearchDbContext(_configuration, new DbContextOptionsBuilder<FlightSearchDbContext>().Options);
        }
    }
}
