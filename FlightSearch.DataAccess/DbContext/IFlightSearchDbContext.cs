﻿using FlightSearch.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess
{
    public interface IFlightSearchDbContext : IDataAccess, IDisposable
    {
        DbSet<Airport> Airports { get; set; }

        DbSet<Country> Countries { get; set; }
        
        DbSet<Day> Days { get; set; }

        DbSet<Flight> Flights { get; set; }

        DbSet<FlightDay> FlightDays { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

        int SaveChanges();
    }
}
