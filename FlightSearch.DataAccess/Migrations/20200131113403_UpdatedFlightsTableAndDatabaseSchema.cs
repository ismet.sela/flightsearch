﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FlightSearch.DataAccess.Migrations
{
    public partial class UpdatedFlightsTableAndDatabaseSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FlightDay_Days_DayId",
                table: "FlightDay");

            migrationBuilder.DropForeignKey(
                name: "FK_FlightDay_Flights_FlightId",
                table: "FlightDay");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FlightDay",
                table: "FlightDay");

            migrationBuilder.RenameTable(
                name: "Days",
                newName: "Days",
                newSchema: "FlightSearch");

            migrationBuilder.RenameTable(
                name: "FlightDay",
                newName: "FlightDays",
                newSchema: "FlightSearch");

            migrationBuilder.RenameIndex(
                name: "IX_FlightDay_FlightId",
                schema: "FlightSearch",
                table: "FlightDays",
                newName: "IX_FlightDays_FlightId");

            migrationBuilder.RenameIndex(
                name: "IX_FlightDay_DayId",
                schema: "FlightSearch",
                table: "FlightDays",
                newName: "IX_FlightDays_DayId");

            migrationBuilder.AlterColumn<int>(
                name: "OriginAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DestinationAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_FlightDays",
                schema: "FlightSearch",
                table: "FlightDays",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FlightDays_Days_DayId",
                schema: "FlightSearch",
                table: "FlightDays",
                column: "DayId",
                principalSchema: "FlightSearch",
                principalTable: "Days",
                principalColumn: "DayId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FlightDays_Flights_FlightId",
                schema: "FlightSearch",
                table: "FlightDays",
                column: "FlightId",
                principalSchema: "FlightSearch",
                principalTable: "Flights",
                principalColumn: "FlightId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FlightDays_Days_DayId",
                schema: "FlightSearch",
                table: "FlightDays");

            migrationBuilder.DropForeignKey(
                name: "FK_FlightDays_Flights_FlightId",
                schema: "FlightSearch",
                table: "FlightDays");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FlightDays",
                schema: "FlightSearch",
                table: "FlightDays");

            migrationBuilder.RenameTable(
                name: "Days",
                schema: "FlightSearch",
                newName: "Days");

            migrationBuilder.RenameTable(
                name: "FlightDays",
                schema: "FlightSearch",
                newName: "FlightDay");

            migrationBuilder.RenameIndex(
                name: "IX_FlightDays_FlightId",
                table: "FlightDay",
                newName: "IX_FlightDay_FlightId");

            migrationBuilder.RenameIndex(
                name: "IX_FlightDays_DayId",
                table: "FlightDay",
                newName: "IX_FlightDay_DayId");

            migrationBuilder.AlterColumn<int>(
                name: "OriginAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DestinationAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FlightDay",
                table: "FlightDay",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FlightDay_Days_DayId",
                table: "FlightDay",
                column: "DayId",
                principalTable: "Days",
                principalColumn: "DayId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FlightDay_Flights_FlightId",
                table: "FlightDay",
                column: "FlightId",
                principalSchema: "FlightSearch",
                principalTable: "Flights",
                principalColumn: "FlightId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
