﻿using FlightSearch.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlightSearch.DataAccess.Models
{
    [Table(name: "Airports", Schema = Constants.DatabaseSchemaName)]
    public class Airport
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AirportId { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string City { get; set; }

        public string Description { get; set; }

        public int CountryId { get; set; }
        
        public Country Country { get; set; }

        public List<Flight> OriginFlights { get; set; }

        public List<Flight> DestinationFlights { get; set; }
    }
}
