﻿using FlightSearch.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlightSearch.DataAccess.Models
{
    [Table(name: "Countries", Schema = Constants.DatabaseSchemaName)]
    public class Country
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Airport> Airports { get; set; }
    }
}
