﻿using FlightSearch.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FlightSearch.DataAccess.Models
{
    
    [Table(name: "Flights", Schema = Constants.DatabaseSchemaName)]
    public class Flight
    {
        [Key]
        public int FlightId { get; set; }

        [Required]
        public string Code { get; set; }

        public int OriginAirportId { get; set; }

        public Airport OriginAirport { get; set; }

        public int DestinationAirportId { get; set; }
        
        public Airport DestinationAirport { get; set; }

        public string DepartureTime { get; set; }

        public string LandingTime { get; set; }

        public string Agency { get; set; }

        public List<FlightDay> FlightDays { get; set; }

        public int Rows { get; set; }

        public int Columns { get; set; }

        public int Price { get; set; }
  }
}
