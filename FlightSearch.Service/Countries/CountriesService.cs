﻿using AutoMapper;
using FlightSearch.DataAccess.Countries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.Service.Countries
{
    public class CountriesService : ICountriesService
    {
        private readonly ICountriesDataAccess _countriesDataAccess;

        private readonly IMapper _mapper;

        public CountriesService(ICountriesDataAccess countriesDataAccess, IMapper mapper)
        {
            _countriesDataAccess = countriesDataAccess;
            _mapper = mapper;
        }

        public async Task<List<CountryDomain>> GetAllAsync()
        {
            var countries = await _countriesDataAccess.FindAllAsync();
            var result = _mapper.Map<List<CountryDomain>>(countries);
            return result;
        }
    }
}
