﻿using AutoMapper;
using FlightSearch.Common;
using FlightSearch.Common.Settings;
using FlightSearch.DataAccess.Airports;
using FlightSearch.DataAccess.Flights;
using FlightSearch.DataAccess.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightSearch.Service.Flights
{
    public class FlightsService : IFlightsService
    {
        private readonly IFlightsDataAccess _flightsDataAccess;
        private readonly IAirportsDataAccess _airportsDataAccess;
        private readonly IMapper _mapper;
        protected AppSettings _appSettings { get; private set; }

        public FlightsService(IFlightsDataAccess flightsDataAccess, IMapper mapper, IAirportsDataAccess airportsDataAccess, IOptions<AppSettings> appSettings)
        {
            _flightsDataAccess = flightsDataAccess;
            _airportsDataAccess = airportsDataAccess;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        public async Task<FlightsCombinationDomain> GetByOriginAndDestinationAsync(int originId, int destinationId)
        {
            var flightsCombination = new FlightsCombinationDomain();
            var directFlights = await _flightsDataAccess.FindByOriginAndDestinationAsync(originId, destinationId);
            var connectingFlights = await GetConnectingFlightsAsync(originId, destinationId);

            flightsCombination.DirectFlights = _mapper.Map<List<FlightUpdateDomain>>(directFlights);
            flightsCombination.ConnectingFlights = connectingFlights;

            if (connectingFlights.Count > 0)
            {
                var featuredFlight = await GetFeaturedFlightAsync(connectingFlights);

                flightsCombination.FeaturedFlight = featuredFlight;
                return flightsCombination;
            }

            return flightsCombination;
        }

        public async Task<FlightDomain> GetByIdAsync(int id)
        {
            var flight = await _flightsDataAccess.FindByIdAsync(id);
            return _mapper.Map<FlightDomain>(flight);
        }

        public async Task RemoveAsync(int id)
        {
            await _flightsDataAccess.DeleteFlightAsync(id);
        }

        public async Task<FlightUpdateDomain> EditAsync(int id, FlightDomain flightDto)
        {
            var foundFlight = await _flightsDataAccess.FindByIdAsync(id);
            if (foundFlight == null)
            {
                throw new Exception(_appSettings.NoFlight);
            }
            if (flightDto.OriginAirportId != 0)
            {
                var originAirport = await _airportsDataAccess.FindById(flightDto.OriginAirportId);
                if (originAirport != null)
                {
                    foundFlight.OriginAirportId = flightDto.OriginAirportId;
                    foundFlight.OriginAirport = originAirport;
                }
            }
            if (flightDto.DestinationAirportId != 0)
            {
                var destinationAirport = await _airportsDataAccess.FindById(flightDto.DestinationAirportId);
                if (destinationAirport != null)
                {
                    foundFlight.DestinationAirportId = flightDto.DestinationAirportId;
                    foundFlight.DestinationAirport = destinationAirport;
                }
            }

            if (flightDto.Columns != 0)
            {
                foundFlight.Columns = flightDto.Columns;
            }
            if (flightDto.Rows != 0)
            {
                foundFlight.Rows = flightDto.Rows;
            }
            if (flightDto.Price != 0)
            {
                foundFlight.Price = flightDto.Price;
            }
            if (!string.IsNullOrWhiteSpace(flightDto.LandingTime))
            {
                foundFlight.LandingTime = flightDto.LandingTime;
            }
            if (!string.IsNullOrWhiteSpace(flightDto.DepartureTime))
            {
                foundFlight.DepartureTime = flightDto.DepartureTime;
            }
            if (!string.IsNullOrWhiteSpace(flightDto.Agency))
            {
                foundFlight.Agency = flightDto.Agency;
            }
            if (!string.IsNullOrWhiteSpace(flightDto.Code))
            {
                var foundByCode = await _flightsDataAccess.FindByCodeAsync(flightDto.Code);
                if (foundByCode != null)
                {
                    throw new Exception(_appSettings.FoundCode);
                }
                foundFlight.Code = flightDto.Code;
            }
            await _flightsDataAccess.EditAsync(id, foundFlight);
            if (flightDto.Days != null && flightDto.Days.Count > 0)
            {
                await _flightsDataAccess.RemoveFlightDaysAsync(id);
                await _flightsDataAccess.AddFlightDaysToFlightAsync(id, flightDto.Days);
            }
            var updatedFlight = await _flightsDataAccess.FindByIdAsync(id);
            return _mapper.Map<FlightUpdateDomain>(updatedFlight);
        }

        public async Task<FlightUpdateDomain> CreateAsync(FlightDomain flightDto)
        {
            var originAirport = await _airportsDataAccess.FindById(flightDto.OriginAirportId);
            if (originAirport == null)
            {
                throw new Exception(_appSettings.NoOrigin);
            }
            var destinationAirport = await _airportsDataAccess.FindById(flightDto.DestinationAirportId);
            if (destinationAirport == null)
            {
                throw new Exception(_appSettings.NoDestination);
            }
            var foundByCode = await _flightsDataAccess.FindByCodeAsync(flightDto.Code);
            if (foundByCode != null)
            {
                throw new Exception(_appSettings.FoundCode);
            }
            var flight = new Flight
            {
                Code = flightDto.Code,
                OriginAirportId = flightDto.OriginAirportId,
                DestinationAirportId = flightDto.DestinationAirportId,
                Columns = flightDto.Columns,
                Rows = flightDto.Rows,
                Price = flightDto.Price,
                LandingTime = flightDto.LandingTime,
                DepartureTime = flightDto.DepartureTime,
                Agency = flightDto.Agency
            };
            var newFlight = await _flightsDataAccess.InsertAsync(flight);
            if (flightDto.Days != null && flightDto.Days.Count > 0)
            {
                await _flightsDataAccess.AddFlightDaysToFlightAsync(newFlight.FlightId, flightDto.Days);
            }
            var insertedFlight = await _flightsDataAccess.FindByIdAsync(newFlight.FlightId);
            return _mapper.Map<FlightUpdateDomain>(insertedFlight);
        }

        public async Task<List<ConnectingFlightsDomain>> GetConnectingFlightsAsync(int originId, int destinationId)
        {

            var flightsByOrigin = await _flightsDataAccess.FindByOriginAsync(originId);
            var flightsByDestination = await _flightsDataAccess.FindByDestinationAsync(destinationId);

            var connectingFlights = new List<ConnectingFlightsDomain>();

            foreach (var originFlight in flightsByOrigin)
            {
                foreach (var destinationFlight in flightsByDestination)
                {
                    if (originFlight.DestinationAirportId == destinationFlight.OriginAirportId)
                    {
                        var indirectFlight = new ConnectingFlightsDomain
                        {
                            OriginFlight = _mapper.Map<FlightUpdateDomain>(originFlight),
                            DestinationFlight = _mapper.Map<FlightUpdateDomain>(destinationFlight)
                        };
                        connectingFlights.Add(indirectFlight);
                    }
                }
            }
            return connectingFlights;
        }

        public IntersectFlights GetOptimalConnectingFlights(ConnectingFlightsDomain indirectFlight)
        {
            var originLandingTime = TimeSpan.Parse(indirectFlight.OriginFlight.LandingTime);
            var destinationDepartureTime = TimeSpan.Parse(indirectFlight.DestinationFlight.DepartureTime);
            int minimalTime;
            int daysDifference;
            int minDayDifference = 8;
            int departureDay = 0;

            foreach (var originDay in indirectFlight.OriginFlight.FlightDays)
            {
                foreach (var destinationDay in indirectFlight.DestinationFlight.FlightDays)
                {
                    if (originDay.Day.DayId == destinationDay.Day.DayId)
                    {
                        daysDifference = 0;
                        if (destinationDepartureTime < originLandingTime )
                        {
                            daysDifference = Constants.NumOfWeekDays;
                        }

                    }
                    else if (originDay.Day.DayId < destinationDay.Day.DayId)
                    {
                        daysDifference = destinationDay.Day.DayId - originDay.Day.DayId;
                    }
                    else
                    {
                        daysDifference = Constants.NumOfWeekDays - originDay.Day.DayId + destinationDay.Day.DayId;
                    }

                    var startTime = TimeSpan.Parse(indirectFlight.OriginFlight.DepartureTime);
                    var endTime = TimeSpan.Parse(indirectFlight.OriginFlight.LandingTime);
                    if (startTime > endTime)
                    {
                        // case when landing day is day after departure
                        if (daysDifference == 0)
                            daysDifference = Constants.NumOfWeekDays - 1;
                        else
                            daysDifference -= 1;
                    }

                    if (daysDifference < minDayDifference)
                    {
                        minDayDifference = daysDifference;
                        departureDay = originDay.Day.DayId;
                    }
                }
            }

            
            minimalTime = Helper.CalculateTimeDifference(indirectFlight.OriginFlight.LandingTime, indirectFlight.DestinationFlight.DepartureTime, minDayDifference);
            
            IntersectFlights intersectFlights = new IntersectFlights
            {
                Day = departureDay,
                OriginId = indirectFlight.OriginFlight.Id,
                DestinationId = indirectFlight.DestinationFlight.Id,
                TimeDifference = minimalTime
            };
            return intersectFlights;
        }

        public List<IntersectFlights> GetAllOptimalIntersectFlights(List<ConnectingFlightsDomain> indirectFlights)
        {
            List<IntersectFlights> flights = new List<IntersectFlights>();
            foreach (var indirectFlight in indirectFlights)
            {
                IntersectFlights interesctFlights = GetOptimalConnectingFlights(indirectFlight);
                flights.Add(interesctFlights);
            }
            return flights;
        }

        public async Task<FeaturedFlight> GetFeaturedFlightAsync(List<ConnectingFlightsDomain> indirectFlights)
        {
            var optimalConnectedFlights = GetAllOptimalIntersectFlights(indirectFlights);
            var optimalFlight = optimalConnectedFlights.OrderBy(f => f.TimeDifference).First();

            var originFlight = await _flightsDataAccess.FindByIdAsync(optimalFlight.OriginId);
            var destinationFlight = await _flightsDataAccess.FindByIdAsync(optimalFlight.DestinationId);

            var connectingFlights = new ConnectingFlightsDomain();
            connectingFlights.OriginFlight = _mapper.Map<FlightUpdateDomain>(originFlight);
            connectingFlights.DestinationFlight = _mapper.Map<FlightUpdateDomain>(destinationFlight);

            var featuredFlight = new FeaturedFlight();
            featuredFlight.FlightCombination = connectingFlights;
            featuredFlight.Day = optimalFlight.Day;

            return featuredFlight;
        }
    }
}
