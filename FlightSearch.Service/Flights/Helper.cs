﻿using FlightSearch.Common;
using System;
using System.Globalization;

namespace FlightSearch.Service.Flights
{
    public static class Helper
    {
        public static int CalculateTimeDifference(string originLandingTime, string destinationDepartureTime, int daysDifference)
        {
            TimeSpan timeDifference = DateTime.Parse(destinationDepartureTime).Subtract(DateTime.Parse(originLandingTime));
            int totalMinutes = (int)timeDifference.TotalMinutes;
            return daysDifference * 24 * 60 + totalMinutes;
        }
    }
}
