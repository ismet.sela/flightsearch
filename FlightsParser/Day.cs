﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightsParser
{
    public class Day
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Day(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }
    }
}
