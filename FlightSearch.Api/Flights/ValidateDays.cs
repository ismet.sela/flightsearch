﻿using FlightSearch.Common;
using FlightSearch.Common.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace FlightSearch.Api.Flights
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    sealed public class ValidateDays : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            if(value == null)
            {
                return true;
            }

            IList objectList = value as IList;
            List<int> list = objectList as List<int>;
           
            int count = list.Count;
            if (count > Constants.NumOfWeekDays)
            {
                return false;
            }
            if (count != list.Distinct().Count())
            {
                return false;
            }
            if (list.Any(d => d < Constants.MinNumOfDays || d > Constants.NumOfWeekDays))
            {
                return false;
            }
            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
            ErrorMessageString, name) + "Days must be between monday and sunday. Can not have duplicate days.";
        }
    }
}
