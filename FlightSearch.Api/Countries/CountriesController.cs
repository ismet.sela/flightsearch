﻿using FlightSearch.Api.Application;
using FlightSearch.Common.Settings;
using FlightSearch.Service.Countries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace FlightSearch.Api.Countries
{
    public class CountriesController : AbstractController
    {
        private ICountriesService _countriesService;

        public CountriesController(
            IOptions<AppSettings> appSettings,
            ICountriesService countriesService)
            : base(appSettings)
        {
            _countriesService = countriesService;
        }

        [HttpGet("all")]
        public async Task<IActionResult> Get()
        {
            var countries = await _countriesService.GetAllAsync();
            return Ok(countries);
        }
    }
}
