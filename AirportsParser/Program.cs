﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AirportsParser
{
    public class Program
    {
        public static void WriteAirportsToTxt(string inputStream, string outputStream)
        {
            StreamReader streamReader = null;
            StreamWriter streamWriter = null;

            try
            {
                streamReader = new StreamReader(inputStream);
                streamWriter = new StreamWriter(outputStream);

                while (true)
                {
                    string line = streamReader.ReadLine();

                    if (line == null)
                    {
                        break;
                    }
                    string trimmedLine = line.Replace("\"", "");
                    string[] array = trimmedLine.Split(",");

                    string code = null;
                    string city = null;
                    string country = null;
                    string description = null;

                    //lenght 4
                    if (array.Length == 4)
                    {
                        city = array[0];
                        description = array[1].Trim();
                        country = array[2].Trim();
                        code = array[3];
                    }
                    //length 3
                    else if (array.Length == 3)
                    {
                        city = array[0];
                        country = array[1].Trim();
                        code = array[2];

                        if (array[1].Contains('-'))
                        {
                            string[] countryDescription = array[1].Split('-');
                            country = countryDescription[0].Trim(); ;
                            description = countryDescription[1].Trim();
                        }
                    }
                    //length 2
                    else
                    {
                        country = array[0];
                        if (array[0].Contains('-'))
                        {
                            string[] countryDescription = array[0].Split('-');
                            country = countryDescription[0].Trim();
                            description = countryDescription[1].Trim();
                        }
                        code = array[1];
                    }
                    string writeLine = code + "," + city + "," + description + "," + country;
                    Console.WriteLine(writeLine);
                    streamWriter.WriteLine(writeLine);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Close();
                }
                if (streamWriter != null)
                {
                    streamWriter.Close();
                }
            }
        }

        public static void WriteCountriesToTxt(string inputStream, string outputStream)
        {
            StreamReader sr = null;
            StreamWriter sw = null;

            try
            {
                sr = new StreamReader(inputStream);
                sw = new StreamWriter(outputStream);

                while (true)
                {
                    string countryLine = sr.ReadLine();

                    if (countryLine == null)
                    {
                        break;
                    }

                    string trimmedLine = countryLine.Replace("\"", "").Replace("\'", "");
                    string[] array = trimmedLine.Split(",");
                    string country = null;

                    //length 4
                    if (array.Length == 4)
                    {
                        country = array[2].Trim();
                    }
                    //length 3
                    else if (array.Length == 3)
                    {
                        country = array[1].Trim();
                        if (array[1].Contains('-'))
                        {
                            string[] countryDescription = array[1].Split('-');
                            country = countryDescription[0].Trim();
                        }
                    }
                    //length 2
                    else
                    {
                        country = array[0];
                        if (array[0].Contains('-'))
                        {
                            string[] countryDescription = array[0].Split('-');
                            country = countryDescription[0].Trim();
                        }
                    }
                    string writeLine = country;
                    sw.WriteLine(writeLine);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        //Function for writing countries and airports as sql inputs in text file
        public static void WriteCountriesAndAirports(string inputCountries, string inputAirports, string outputCountries, string otuputAirports)
        {
            StreamReader countriesReader = null;
            StreamReader airportsReader = null;
            StreamWriter countriesWriter = null;
            StreamWriter airportsWriter = null;

            try
            {
                countriesReader = new StreamReader(inputCountries);
                airportsReader = new StreamReader(inputAirports);
                countriesWriter = new StreamWriter(outputCountries);
                airportsWriter = new StreamWriter(otuputAirports);

                string countryLine = countriesReader.ReadLine();
                List<string> countries = new List<string>();
                List<string> countriesWithId = new List<string>();
                int counter = 0;
                string sqlRowCountry = "";
                while (countryLine != null)
                {
                    if (!countries.Contains(countryLine))
                    {
                        counter++;
                        countriesWithId.Add(counter + "," + countryLine);
                        countries.Add(countryLine);
                        sqlRowCountry = $"INSERT INTO [FlightSearch].[Countries] ([CountryId], [Name]) VALUES (" + counter + ",\'" + countryLine + "\')\n";
                        countriesWriter.Write(sqlRowCountry);
                    }
                    countryLine = countriesReader.ReadLine();
                }

                string airportLine = airportsReader.ReadLine();
                while (airportLine != null)
                {
                    airportLine = airportLine.Replace("\'", "\'\'");
                    foreach (var item in countriesWithId)
                    {
                        string[] country = item.Split(",");
   
                        if (airportLine.Contains(country[1]))
                        {
                            string[] airport = airportLine.Split(",");
                            string sqlRowAirport = $"INSERT INTO [FlightSearch].[Airports] ([Code], [City], [Description], [CountryId]) VALUES (\'" + airport[0] + "\',\'" + airport[1] + "\',\'" + airport[2] + "\'," + country[0] + ")\n";
                            airportsWriter.Write(sqlRowAirport);
                            break;
                        }
                    }
                    airportLine = airportsReader.ReadLine();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                countriesReader.Close();
                airportsReader.Close();
                countriesWriter.Close();
                airportsWriter.Close();
            }
        }

        static void Main(string[] args)
        {
            string airportCodes = @"../../../..//Tools/Files/airport-codes.csv";
            string airports = @"../../../..//Tools/Files/airport.txt";
            string countries = @"../../../..//Tools/Files/countries.txt";
            string countriesSql = @"../../../..//Tools/Files/countriesSql.txt";
            string airportsSql = @"../../../..//Tools/Files/airportsSql.txt";
            WriteAirportsToTxt(airportCodes, airports);
            WriteCountriesToTxt(airportCodes, countries);
            WriteCountriesAndAirports(countries, airports, countriesSql, airportsSql);
        }
    }
}
